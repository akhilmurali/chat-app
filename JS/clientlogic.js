// Client side js script.
// Upon form submission it emits an event named 'chat message'
//The event emitter is passed to the client side via the socket.
//The client side captures event and updates the UI client app.
$(function () {
    var socket = io();
    console.log("client side js invoked");
    $('form').submit(function(){
        socket.emit('chat message', $('#m').val());
     
        $('#m').val('');
        return false;
    });
    socket.on('chat message', function(msg){
        $('#messages').append($('<li>').text(msg));
        console.log("socket code on client invoked");
    });
});