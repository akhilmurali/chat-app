// A simple chat application that uses socket.io and node js.
//Include an instantiate external libraries into your project.
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

// Route call to the home directory
app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

app.get('/JS/clientlogic.js', function(req, res){
    res.sendFile(__dirname + '/JS/clientlogic.js');
});

// Server captures the event sent via the socket to the server.
// Upon capture the server broadcasts the event to all connected clients.
// The clients upon capture of event via the socket update their UI.
io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('chat message', function(msg){
        io.emit('chat message', msg);
        console.log("socket code on server invoked");
    });
    socket.on('disconnect', function(){
        console.log('user disconnected');
      });
  });

http.listen(3000, function(){
  console.log('listening on *:3000');
});
